DROP  DATABASE IF EXISTS hotel;
CREATE DATABASE hotel;

use hotel;

drop  table if exists bookings;
drop table if exists  users;
drop table if exists  roles;
drop table if exists  rooms;

GO
CREATE TABLE roles 
(
 id INT PRIMARY KEY NOT NULL,
 name VARCHAR(20) NOT NULL,
);

INSERT INTO roles (id,name)VALUES (1,'CLIENTE'), (2,'ADMINISTRADOR');

Create table users(
id bigint identity primary key,
ident_user varchar(11) not null,
name_user varchar(300) not null,
user_email varchar(100) unique not null,
password_user varchar(250) not null,
role_user int not null default 1,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP
CONSTRAINT fk_roles FOREIGN KEY (role_user) REFERENCES roles(id) 
);


Create table rooms(
id bigint identity primary key,
num_room varchar(5) unique not null,
type_room varchar(50) not null,
cost_room money not null ,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);


Create table bookings(
id bigint identity primary key,
user_email varchar(100) not null,
count_people int not null,
num_room varchar(5) not null,
date_checkin date not null,
date_checkout date not null,
created_at DATETIME DEFAULT CURRENT_TIMESTAMP);


Alter table bookings add constraint Fk_useremail
foreign key(user_email) references users(user_email);


Alter table bookings add constraint Fk_room
foreign key(num_room) references rooms(num_room);



--insert data
--SELECT * FROM users;
--SELECT * FROM roles;
--SELECT * FROM bookings;

--insert admin, pass  12345678
INSERT INTO users (ident_user,name_user,user_email,password_user,role_user) VALUES ('3-0433-0789','ADMIN','admin@gmail.com','AHZQMWbLWgs77+tt3VJazu7feVy86GLgnSeZpUjqq3E9GC138t5sFOjkJMH8LQvmTw==',2);
--insert client, pass  12345678
INSERT INTO users (ident_user,name_user,user_email,password_user,role_user) VALUES ('3-0433-0789','CLIENT','client@gmail.com','AHZQMWbLWgs77+tt3VJazu7feVy86GLgnSeZpUjqq3E9GC138t5sFOjkJMH8LQvmTw==',1);

INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-789','DOBLE',25000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-788','DOBLE',25000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-787','INDIVIDUAL',15000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-786','DOBLE',25000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-785','DOBLE',25000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-784','INDIVIDUAL',15000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-783','DOBLE',25000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-782','INDIVIDUAL',15000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-781','DOBLE',25000);
INSERT INTO rooms (num_room,type_room,cost_room) VALUES ('H-780','INDIVIDUAL',15000);



	INSERT INTO bookings (user_email,count_people,num_room,date_checkin,date_checkout)
	VALUES
	('client@gmail.com',2,'H-789','2021-02-01','2021-03-11'),
	('client@gmail.com',2,'H-789','2021-03-14','2021-03-21'),
	('client@gmail.com',2,'H-788','2021-03-01','2021-04-15'),
	('client@gmail.com',2,'H-789','2021-03-01','2021-03-18'),
	('client@gmail.com',1,'H-782','2021-03-01','2021-03-24'),
	('client@gmail.com',2,'H-781','2021-03-01','2021-03-29'),
	('client@gmail.com',2,'H-789','2021-04-01','2021-04-03');

	

--OBTENER HABITACIONES DISPONIBLES
SELECT * FROM rooms h WHERE NOT EXISTS 
(SELECT * FROM bookings WHERE
	num_room= h.num_room and  date_checkin  between '2021-03-30' and '2021-03-31'
    or  num_room=  h.num_room and  date_checkout  between '2021-03-12' and '2021-03-15'
	 ) ;

