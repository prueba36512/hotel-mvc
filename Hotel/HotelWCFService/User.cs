﻿using HotelWCFService.Persistence;
using HotelWCFService.Repository;
using System.Linq;


namespace HotelWCFService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "User" en el código y en el archivo de configuración a la vez.
    public class User : IUser
    {
        Bussines bussiness = new Bussines();
        public int createUser(UserRepository newUser)
        {
            if (!bussiness.tUsers.Any(u => u.user_email == newUser.user_email))
            {
                bussiness.tUsers.InsertOnSubmit(newUser);
                bussiness.SubmitChanges();
                return UserRepository.CREATED;
            }
            else
            {
                return UserRepository.USER_EMAIL_EXISTS;
            }
        }

        public UserRepository getUser(string user_email)
        {
            return bussiness.tUsers.Where(u => u.user_email == user_email).FirstOrDefault();
        }
    }
}
