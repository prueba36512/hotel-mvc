﻿using HotelWCFService.Persistence;
using HotelWCFService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HotelWCFService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Room" en el código y en el archivo de configuración a la vez.
    public class Room : IRoom
    {

        Bussines bussiness = new Bussines();
        public List<RoomRepository> getRooms(DateTime date_checkin, DateTime date_checkout)
        {
            var query = from r in bussiness.tRooms
                        where !bussiness.tBookings.Any(bk =>
                        (bk.num_room == r.num_room && bk.date_checkin >= date_checkin &&
                        bk.date_checkin <= date_checkout)
                        ||
                         (bk.num_room == r.num_room && bk.date_checkout >= date_checkin &&
                        bk.date_checkout <= date_checkout)
                        )
                        select r;

            return query.ToList();
        }
    }
}
