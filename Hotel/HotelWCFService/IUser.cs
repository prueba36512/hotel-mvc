﻿using HotelWCFService.Repository;
using System.ServiceModel;

namespace HotelWCFService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IUser" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IUser
    {
        [OperationContract]
        int createUser(UserRepository newUser);

        [OperationContract]
        UserRepository getUser(string user_email);
    }
}
