﻿using HotelWCFService.Repository;
using System;
using System.Collections.Generic;
using System.ServiceModel;


namespace HotelWCFService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IBooking" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IBooking
    {
        [OperationContract]
        int createBooking(BookingRepository newBooking);

        [OperationContract]
        List<BookingRepository> getBookings(Nullable<DateTime> date_checkin, Nullable<DateTime> date_checkout, string user_email, bool bookingsByUser);
    }
}
