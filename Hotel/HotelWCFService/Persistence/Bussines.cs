using HotelWCFService.Repository;
using System.Data.Linq;
using System.Data.Linq.Mapping;


namespace HotelWCFService.Persistence
{
    [Database(Name = "hotel")]
    public class Bussines : DataContext
    {
        public Bussines() : base(Configuration.connectionString) { }
        public Table<UserRepository> tUsers;//obj de la tabla en memoria
        public Table<RoomRepository> tRooms;
        public Table<BookingRepository> tBookings;
    }
}
