﻿using System.Configuration;

namespace HotelWCFService.Persistence
{
    class Configuration
    {
        public static string connectionString
        {
            get
            {
                return ConfigurationManager. //la configuracion va en web.config
                    ConnectionStrings["connectionString"].
                    ConnectionString.
                    ToString();
            }
        }
    }
}
