﻿using HotelWCFService.Repository;
using System;
using System.Collections.Generic;
using System.ServiceModel;


namespace HotelWCFService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IRoom" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IRoom
    {
        [OperationContract]
        List<RoomRepository> getRooms(DateTime date_checkin, DateTime date_checkout);
    }
}
