﻿using System.Data.Linq.Mapping;

namespace HotelWCFService.Repository
{
    [Table(Name = "users")]//table name
    public class UserRepository
    {
        public const int CLIENT = 1;
        public const int ADMINISTRATOR = 2;
        public const int CREATED = 1;
        public const int USER_EMAIL_EXISTS = 2;

        [Column(Name ="id",IsPrimaryKey = true, IsDbGenerated = true)]
        public long id { set; get; }

        [Column(Name = "ident_user")]
        public string ident_user { set; get; }

        [Column(Name = "name_user")]
        public string name_user { set; get; }

        [Column(Name = "user_email")]
        public string user_email { set; get; }

        [Column(Name = "password_user")]
        public string password_user { set; get; }

        [Column(Name = "role_user")]
        public int role_user { set; get; }

       
    }
}
