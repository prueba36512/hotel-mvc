﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelWCFService.Repository
{
     [Table(Name = "bookings")]
    public class BookingRepository
    {
        public const int CREATED = 1;

        [Column(Name = "id", IsPrimaryKey = true, IsDbGenerated = true)]
        public long id;

        [Column(Name = "user_email")]
        public string user_email;

        [Column(Name = "count_people")]
        public int count_people;

        [Column(Name = "num_room")]
        public string num_room;

        [Column(Name = "date_checkin")]
        public DateTime date_checkin;

        [Column(Name = "date_checkout")]
        public DateTime date_checkout;

        [Column(Name = "created_at", IsDbGenerated = true)]
        public Nullable<DateTime> created_at;
    }
}
