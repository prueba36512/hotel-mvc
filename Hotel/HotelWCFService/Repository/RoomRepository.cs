﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelWCFService.Repository
{
    [Table(Name = "rooms")]//table name
    public class RoomRepository
    {
        [Column(Name = "id", IsPrimaryKey = true, IsDbGenerated = true)]
        public long id;

        [Column(Name = "num_room")]
        public string num_room;

        [Column(Name = "type_room")]
        public string type_room;

        [Column(Name = "cost_room")]
        public decimal cost_room;
    }
}
