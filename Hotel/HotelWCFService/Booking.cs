﻿using HotelWCFService.Persistence;
using HotelWCFService.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HotelWCFService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Booking" en el código y en el archivo de configuración a la vez.
    public class Booking : IBooking
    {
        Bussines bussiness = new Bussines();

        public int createBooking(BookingRepository newBooking)
        {
            bussiness.tBookings.InsertOnSubmit(newBooking);
            bussiness.SubmitChanges();
            return BookingRepository.CREATED;
        }

        public List<BookingRepository> getBookings(Nullable<DateTime> date_checkin, Nullable<DateTime> date_checkout, string user_email, bool bookingsByUser)
        {
            var query = from bk in bussiness.tBookings
                        
                        select bk ;
            if (bookingsByUser)
            {
                query = query.Where(bk => bk.user_email == user_email);
            }

            if (date_checkin != null && date_checkout != null)
            {
                query = query.Where(bk => bk.date_checkin >= date_checkin && bk.date_checkout <= date_checkout);
            }
            else
            if (date_checkin != null)
            {
                query = query.Where(bk => bk.date_checkin >= date_checkin);
            }
            else
            if (date_checkout != null)
            {
                query = query.Where(bk => bk.date_checkout <= date_checkout);
            }

            query = query.OrderByDescending(bk=> bk.created_at);

            return query.ToList();
        }
    }
}
