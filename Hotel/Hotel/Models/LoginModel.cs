﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Helpers;

namespace Hotel.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Correo electrónico es requerido")]
        public string user_email { get; set; }

        [Required(ErrorMessage = "Contraseña es requerido")]
        public string password_user { get; set; }

        public static void createUserSession(UserModel user) {
           HttpContext.Current.Session["user"] = user;
        }

        public static UserModel getUserSession()
        {
            UserModel user= (UserModel)HttpContext.Current.Session["user"];
            return user;
        }

        public static bool validUser(string password,string password_hash)
        {
            return password_hash==null ? false : Crypto.VerifyHashedPassword(password_hash, password);
        }

        public static void  closeUserSession()
        {
            HttpContext.Current.Session["user"]=null;
        }
    }
}