﻿using Hotel.BookingService;
using System;


namespace Hotel.Models
{
    public class BookingModel
    {
   
        public Nullable<DateTime> date_checkin { get; set; }
        public Nullable<DateTime> date_checkout { get; set; }

        public BookingRepository[] BookinList { get; set; }
    }
}