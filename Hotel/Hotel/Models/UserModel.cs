﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace Hotel.Models
{
    public class UserModel
    {
        public const int CLIENT = 1;
        public const int ADMINISTRATOR = 2;
        public const int CREATED = 1;
        public const int USER_EMAIL_EXISTS = 2;

        [Required(ErrorMessage = "Identificación es requerido")]
        [RegularExpression("^\\d{1}-\\d{4}-\\d{4}$", ErrorMessage = "Formato de identificación esperado #-####-####")]
        public string ident_user { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(300, ErrorMessage = "Máximo 300 carácteres")]
        public string name_user { get; set; }

        [Required(ErrorMessage = "Correo electrónico es requerido")]
        [StringLength(100, ErrorMessage = "Logitud máxima 100 carácteres")]
        [EmailAddress(ErrorMessage = "Correo electrónico no válido")]
        public string user_email { get; set; }

        [Required(ErrorMessage = "Contraseña es requerido")]
        [StringLength(16, ErrorMessage = "Debe estar entre 8 y 16 carácteres", MinimumLength=8)]
        public string password_user { get; set; }

        [Required(ErrorMessage = "Comfirmar contraseña es requerido")]
        [Compare("password_user",ErrorMessage ="Las contraseñas no coinciden")]
        public string confirmPassword { get; set; }

        public int role_user { set; get; }


        public bool isAdmin()
        {
            return this.role_user == ADMINISTRATOR;
        }

        public bool isClient()
        {
            return this.role_user == CLIENT;
        }

        public string createHashPassword()
        {
            return Crypto.HashPassword(this.password_user);
        }

    }
}