﻿using Hotel.RoomService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class RoomModel : IValidatableObject
    {
        [Required(ErrorMessage = "Entrada es requerido")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime date_checkin { get; set; }

        [Required(ErrorMessage = "Salida es requerido")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime date_checkout { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (date_checkout < date_checkin)
            {
                yield return new ValidationResult(
                    errorMessage: "La fecha de salida debe ser mayor o igual a fecha de entrada",
                    memberNames: new[] { "date_checkout" }
               );
            }
        }

        public RoomRepository[] RoomList { get; set; }
    }
}