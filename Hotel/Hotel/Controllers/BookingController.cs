﻿using Hotel.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Hotel.BookingService;
using System;

namespace Hotel.Controllers
{
    public class BookingController : Controller
    {
        private BookingClient bookingClient = new BookingClient();

        public ActionResult Index()
        {
            BookingModel booking = new BookingModel();
            bool bookingsByUser = LoginModel.getUserSession().isAdmin() ? false : true;
            booking.BookinList = bookingClient.getBookings(booking.date_checkin, booking.date_checkout, LoginModel.getUserSession().user_email, bookingsByUser);

            return View(booking);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(BookingModel booking)
        {

            if (ModelState.IsValid)
            {
                bool bookingsByUser = LoginModel.getUserSession().isAdmin() ? false : true;
                booking.BookinList = bookingClient.getBookings(booking.date_checkin, booking.date_checkout, LoginModel.getUserSession().user_email, bookingsByUser);
            }
            return View(booking);
        }

        [HttpGet]
        public ActionResult Create(string num_room,string type_room,DateTime date_checkin, DateTime date_checkout)
        {
            /*System.Diagnostics.Debug.WriteLine("NUM_ROOM => "+num_room);
            System.Diagnostics.Debug.WriteLine("TYPE_ROOM => " + type_room);
            System.Diagnostics.Debug.WriteLine("date_checkin => " + date_checkin);
            System.Diagnostics.Debug.WriteLine("date_checkout => " + date_checkout);*/

            int count_people = type_room == "INDIVIDUAL" ? 1 : type_room == "DOBLE" ? 2 : 0; 

            BookingRepository bookingRepository = new BookingRepository(){
                user_email= LoginModel.getUserSession().user_email,
                count_people=count_people,
                num_room=num_room,
                date_checkin= date_checkin,
                date_checkout= date_checkout
            };

            int created=bookingClient.createBooking(bookingRepository);

            if (created==1)
            {
                TempData["successMessage"] = "Reservación realizada correctamente";
                return RedirectToAction("Index", "Booking");
            }
            else
            {
                TempData["errorMessage"] = "Error al realizar la reservación";
                return RedirectToAction("Index", "Room");
            }


           
        }
    }
}
