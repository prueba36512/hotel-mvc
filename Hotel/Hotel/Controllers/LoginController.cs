﻿using Hotel.Filters;
using Hotel.UserService;
using Hotel.Models;
using System.Web.Mvc;

namespace Hotel.Controllers
{
    public class LoginController : Controller
    {
        private UserClient userClient = new UserClient();

        [VerifyUserSession(true, null)]//no check user session
        public ActionResult LoginForm()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VerifyUserSession(true,null)]
        public ActionResult LoginForm(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                UserRepository userRepository = userClient.getUser(login.user_email);
                if (userRepository != null)
                {
                    if (LoginModel.validUser(login.password_user, userRepository.password_user))
                    {

                        UserModel user = new UserModel()
                        {
                            ident_user = userRepository.ident_user,
                            name_user = userRepository.name_user,
                            user_email = userRepository.user_email,
                            password_user = userRepository.password_user,
                            role_user = userRepository.role_user
                        };
                        LoginModel.createUserSession(user);
                        return RedirectToAction("Index", "Room");
                    }
                    else
                    {
                        ModelState.AddModelError("password_user", "Correo electrónico o contraseña incorrectos");
                    }
                }
                else
                {
                    ModelState.AddModelError("password_user", "Correo electrónico o contraseña incorrectos");
                }

               
            }
           
            return View();
        }

        public ActionResult closeSession()
        {
            LoginModel.closeUserSession();
            return RedirectToAction("LoginForm", "Login");
        }

    }
}
