﻿using Hotel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hotel.RoomService;
using Hotel.Filters;

namespace Hotel.Controllers
{
    public class RoomController : Controller
    {
        private RoomClient roomClient = new RoomClient();

        [VerifyUserSession(operation: "canCreateBooking")]
        public ActionResult Index()
        {
            RoomModel room = new RoomModel();
            room.date_checkin = DateTime.Now;
            room.date_checkout = DateTime.Now;
            return View(room);
        }

        [VerifyUserSession(operation: "canCreateBooking")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(RoomModel room)
        {

            if (ModelState.IsValid)
            {
                room.RoomList = roomClient.getRooms(room.date_checkin, room.date_checkout);
                if (room.RoomList.Length==0)
                {
                    TempData["infoMessage"] = "No hay habitaciones disponibles";
            
                }

            }
            return View(room);
        }
    }
}
