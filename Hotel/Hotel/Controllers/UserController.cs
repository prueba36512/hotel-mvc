﻿using Hotel.Filters;
using Hotel.UserService;
using Hotel.Models;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Hotel.Controllers
{
    public class UserController : Controller
    {
        private UserClient userClient = new UserClient();

        [VerifyUserSession(true, null)]//no check user session
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VerifyUserSession(true, null)]//no check user session
        public ActionResult Register(UserModel user)
        {
            if (ModelState.IsValid)
            {
               
                int result = userClient.createUser(new UserRepository() {
                    ident_user=user.ident_user,
                    name_user = user.name_user,
                    user_email = user.user_email,
                    password_user = user.createHashPassword(),
                    role_user = UserModel.CLIENT
                });

                if (result==UserModel.USER_EMAIL_EXISTS) {
                    ModelState.AddModelError("user_email","Correo electrónico no disponible");
                }else if (result == UserModel.CREATED){
                    TempData["successMessage"] = "Usuario registrado con éxito, por favor inicie sesión";
                    return RedirectToAction("LoginForm", "Login");
                }
            }
            return View();
        }

    }
}
