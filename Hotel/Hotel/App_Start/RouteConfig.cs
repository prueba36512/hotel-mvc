﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Hotel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Login", "Login/LoginForm",
               new { controller = "Login", action = "LoginForm" }
            );

            routes.MapRoute(
                "Booking", "Booking/index",
               new { controller = "Booking", action = "Index" }
            );

            routes.MapRoute(
                "User", "User/index",
               new { controller = "User", action = "Login" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Room", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
