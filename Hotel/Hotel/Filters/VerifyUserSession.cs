﻿using Hotel.Models;
using System;
using System.Web.Mvc;

namespace Hotel.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class VerifyUserSession: ActionFilterAttribute
    {

        private bool disableFilter =false;
        private string operation;
        public VerifyUserSession(string operation)
        {
            this.operation = operation;
        }
        public VerifyUserSession(bool disableFilter, string operation)
        {
            this.operation = operation;
            this.disableFilter = disableFilter;
        }

        public VerifyUserSession(){}


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (disableFilter) return;
            try {

                if (LoginModel.getUserSession() == null)
                {
                    filterContext.Result = new RedirectResult("~/Login/LoginForm", false);
                }
                else
                {
                    bool isAuthorized = true;
                    UserModel user = LoginModel.getUserSession();
                    switch (operation)
                    {
                        case "canRegisterUser":
                            isAuthorized = (user == null) ? true : false;
                            break;
                        case "canCreateBooking":
                            isAuthorized = (user != null) ? user.isClient() ? true : false : true;
                            break;
                    }

                    if (!isAuthorized)
                    { 
                        filterContext.Result = new RedirectResult("~/Booking/Index",false);
                    }
                }
            }catch(Exception)
            {
                filterContext.Result = new RedirectResult("~/Login/LoginForm",false);
            }
            base.OnActionExecuting(filterContext);
        }
    }
}