﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hotel.RoomService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RoomRepository", Namespace="http://schemas.datacontract.org/2004/07/HotelWCFService.Repository")]
    [System.SerializableAttribute()]
    public partial class RoomRepository : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal cost_roomField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string num_roomField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string type_roomField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal cost_room {
            get {
                return this.cost_roomField;
            }
            set {
                if ((this.cost_roomField.Equals(value) != true)) {
                    this.cost_roomField = value;
                    this.RaisePropertyChanged("cost_room");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long id {
            get {
                return this.idField;
            }
            set {
                if ((this.idField.Equals(value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string num_room {
            get {
                return this.num_roomField;
            }
            set {
                if ((object.ReferenceEquals(this.num_roomField, value) != true)) {
                    this.num_roomField = value;
                    this.RaisePropertyChanged("num_room");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string type_room {
            get {
                return this.type_roomField;
            }
            set {
                if ((object.ReferenceEquals(this.type_roomField, value) != true)) {
                    this.type_roomField = value;
                    this.RaisePropertyChanged("type_room");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="RoomService.IRoom")]
    public interface IRoom {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRoom/getRooms", ReplyAction="http://tempuri.org/IRoom/getRoomsResponse")]
        Hotel.RoomService.RoomRepository[] getRooms(System.DateTime date_checkin, System.DateTime date_checkout);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IRoom/getRooms", ReplyAction="http://tempuri.org/IRoom/getRoomsResponse")]
        System.Threading.Tasks.Task<Hotel.RoomService.RoomRepository[]> getRoomsAsync(System.DateTime date_checkin, System.DateTime date_checkout);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IRoomChannel : Hotel.RoomService.IRoom, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RoomClient : System.ServiceModel.ClientBase<Hotel.RoomService.IRoom>, Hotel.RoomService.IRoom {
        
        public RoomClient() {
        }
        
        public RoomClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RoomClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RoomClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RoomClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Hotel.RoomService.RoomRepository[] getRooms(System.DateTime date_checkin, System.DateTime date_checkout) {
            return base.Channel.getRooms(date_checkin, date_checkout);
        }
        
        public System.Threading.Tasks.Task<Hotel.RoomService.RoomRepository[]> getRoomsAsync(System.DateTime date_checkin, System.DateTime date_checkout) {
            return base.Channel.getRoomsAsync(date_checkin, date_checkout);
        }
    }
}
